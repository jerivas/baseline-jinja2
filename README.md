# Baseline Jinja2

A Jinja2 version of [Baseline for Skel].

The Jinja2 templates are stored in the `templates` folder and are compiled to
the `dist` folder.

The SCSS stylesheets are stored in `dist/assets/sass` and are compiled to
`dist/assets/css`. Everything else inside `dist/assets` is a regular file.

## Prerequisites

```bash
# Install Python requirements
mkvirtualenv baseline-jinja2
pip install -r requirements.txt

# Install node requirements
npm install
```

## Quickstart

```bash
# Wath the templates
python build.py

# Watch the stylesheets
npm run sass
```

[Baseline for Skel]: https://github.com/ajlkn/baseline
