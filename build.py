#!/usr/bin/env python

import json

from staticjinja import make_site


def get_context():
    """
    Load the JSON context data for the templates.
    """
    with open("context.json", "r") as context:
        return json.load(context)

if __name__ == "__main__":
    site = make_site(
        contexts=[
            (r".+\.html", get_context),
        ],
        outpath="dist",
        env_kwargs={"trim_blocks": True, "lstrip_blocks": True}
    )
    # Enable automatic reloading
    site.render(use_reloader=True)
